/*
  merge() need to be called in index.js and in components
*/
const methods = {
  mergeRecursive(o1, o2) {
    const o3 = o1;
    Object.keys(o2).forEach((p) => {
      try {
        if (o2[p].constructor === Object) {
          o3[p] = this.mergeRecursive(o3[p], o2[p]);
        } else if (o2[p].constructor === Array) {
          for (let i = 0; i < o2[p].length; i += 1) {
            o3[p][i] = o2[p][i];
          }
        } else {
          o3[p] = o2[p];
        }
      } catch (e) {
        o3[p] = o2[p];
      }
    });
    return o3;
  },
  merge(a, b) {
    const A = a === undefined ? {} : a;
    const B = b === undefined ? {} : b;

    // Copy String
    if (typeof B === 'string') {
      return B;
    }
    // Merge Arrays
    if (A.constructor === Array && B.constructor === Array) {
      return [...new Set([].concat(...[A, B]))];
    }
    // Merge Objects
    const o1 = Object.assign({}, A);
    const o2 = Object.assign({}, B);
    this.mergeRecursive(o1, o2);
    return o1;
  },
};
export default {
  $: methods.merge,
  mergeRecursive: methods.mergeRecursive,
  install(Vue) {
    Vue.mixin({
      methods,
    });
  },
};
