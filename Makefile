backup-locales:
	mkdir -p tools/backup/
	cp -r src/translations/* tools/backup/

restore-locales:
	cp -r tools/backup/* src/translation/

clean-locales: backup-locales
	cd tools && ./cleanyml.sh

diff-locales:
	(meld src/translations/ tools/backup/ &)
