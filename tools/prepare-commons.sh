#!/bin/bash

# Install packages
npm i

# Add commons symlink
if [[ ! -d commons ]]; then
  ln -s node_modules/vue-fs-commons/src commons
fi

# Import dio translations
if [[ $(cat src/data/main.yml | grep -o 'dio: true') == "dio: true" ]]; then
  for f in $(find ./node_modules/vue-fs-commons/src/translations -name 'dio.yml' -type f); do
    lgdir=$(echo $f | cut -d '/' -f1,4-6);
    if [ -d $lgdir ]; then
      echo $lgdir/dio.yml ✓;
      cp $f $lgdir/dio.yml;
    else
      echo $lgdir/dio.yml ✗;
    fi;
  done;
fi

# Import hos translations
if [[ $(cat src/data/main.yml | grep -o 'hos: true') == "hos: true" ]]; then
  for f in $(find ./node_modules/vue-fs-commons/src/translations -name 'hos.yml' -type f); do
    lgdir=$(echo $f | cut -d '/' -f1,4-6);
    if [ -d $lgdir ]; then
      echo $lgdir/hos.yml ✓;
      cp $f $lgdir/hos.yml;
    else
      echo $lgdir/hos.yml ✗;
    fi;
  done;
fi

# Import sfs translations
if [[ $(cat src/data/main.yml | grep -o 'sfs: true') == "sfs: true" ]]; then
  for f in $(find ./node_modules/vue-fs-commons/src/translations -name 'sfs.yml' -type f); do
    lgdir=$(echo $f | cut -d '/' -f1,4-6);
    if [ -d $lgdir ]; then
      echo $lgdir/sfs.yml ✓;
      cp $f $lgdir/sfs.yml;
    else
      echo $lgdir/sfs.yml ✗;
    fi
  done
fi

# Import wiki
WikiAPI=$(cat src/data/main.yml | grep -Po 'api: \K.*');
WikiBase=$(cat src/data/main.yml | grep -Po 'base: \K.*');
WikiGit=$(echo $WikiBase | rev | cut -d '/' -f4- | rev).wiki.git;
if [[ "$WikiAPI" == *"http"* ]]; then
  # Import wiki.json
  wget "$WikiAPI" -O public/wiki.json

  # Import uploads
  if [ -d public/wiki ]; then
    rm -rf public/wiki
  fi

  mkdir public/wiki
  git clone $WikiGit public/wiki/tmp/

  if [ -d public/wiki/tmp/uploads ]; then
    mv public/wiki/tmp/uploads/* public/wiki/
  fi
  if [ -d public/wiki/tmp/newsletter ]; then
    mv public/wiki/tmp/newsletter public/wiki/
  fi
  rm -rf public/wiki/tmp/
fi
