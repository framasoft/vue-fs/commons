WikiAPI=$(cat src/data/main.yml | grep -Po 'api: \K.*');
WikiBase=$(cat src/data/main.yml | grep -Po 'base: \K.*');
WikiGit=$(echo $WikiBase | rev | cut -d '/' -f4- | rev).wiki.git;

# Import wiki.json
wget "$WikiAPI" -O public/wiki.json

# Import uploads
if [ -d public/wiki ]; then
  rm -rf public/wiki
fi;

mkdir public/wiki
git clone $WikiGit public/wiki/tmp/

if [ -d public/wiki/tmp/uploads ]; then
  mv public/wiki/tmp/uploads/* public/wiki/
fi;
rm -rf public/wiki/tmp/