for f in $(find ./node_modules/vue-fs-commons/src/translations -name 'hos.yml' -type f); do
  lgdir=$(echo $f | cut -d '/' -f1,4-6);
  if [ -d $lgdir ]; then
    echo $lgdir/hos.yml ✓;
    cp $f $lgdir/hos.yml;
  else
    echo $lgdir/hos.yml ✗;
  fi;
done;
